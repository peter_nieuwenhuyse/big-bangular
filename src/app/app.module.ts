import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PersonsListComponent } from './persons/persons-list/persons-list.component';
import { PersonsService } from './persons/persons.service';
import { HttpClientModule } from '@angular/common/http';
import { PersonDetailComponent } from './persons/person-detail/person-detail.component';
import { RouterModule } from '@angular/router';
import { PersonFormComponent } from './persons/person-form/person-form.component';
import { FormsModule } from '@angular/forms';
import { L10nConfig, L10nLoader, TranslationModule, StorageStrategy, ProviderType } from 'angular-l10n';

const l10nConfig: L10nConfig = {
  locale: {
    languages: [
      {code: 'en', dir: 'ltr'},
      {code: 'nl', dir: 'ltr'}
    ],
    language: 'en',
    storage: StorageStrategy.Cookie
  },
  translation: {
    providers: [
      { type: ProviderType.Static, prefix: './assets/locale-'}
    ],
    caching: true,
    missingValue: 'No key'
  }
};

@NgModule({
  declarations: [
    AppComponent,
    PersonsListComponent,
    PersonDetailComponent,
    PersonFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      {path: '', component: PersonsListComponent },
      {path: 'add/:id', component: PersonFormComponent},
      {path: 'add', component: PersonFormComponent},
      {path: ':id', component: PersonDetailComponent}
    ]),
    TranslationModule.forRoot(l10nConfig)
  ],
  providers: [
    PersonsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public l10nLoader: L10nLoader) {
    this.l10nLoader.load();
  }
}
