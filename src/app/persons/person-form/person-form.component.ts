import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PersonsService } from '../persons.service';
import { Person } from '../person';

@Component({
  selector: 'app-person-form',
  templateUrl: './person-form.component.html',
  styleUrls: ['./person-form.component.css']
})
export class PersonFormComponent implements OnInit, OnDestroy {

  person = {};
  personPlaceholder: Person;
  id: string | null;

  constructor(private route: ActivatedRoute,
              private personsService: PersonsService,
              private router: Router) {
    this.id = this.route.snapshot.paramMap.get('id');
    if (this.id) { this.personsService.getPersonDetail(this.id).subscribe(person => this.person = person); }
  }

  ngOnInit() {
  }

  ngOnDestroy() {
  }

  save(person: Person) {
    if (this.id) {
      this.personsService.updatePerson(person);
      this.router.navigate(['/', this.id]);
    } else {
      person.id = person.lastName;
      this.personsService.createPerson(person);
      this.router.navigate(['/']);
    }
  }
}

