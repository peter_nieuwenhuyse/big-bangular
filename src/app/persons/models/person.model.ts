
import {Person} from '../person';

export class PersonModel {
  person: Person;
  lastName: string;
  profession: string;
  bio: string;
  url: string;
  imageUri: string;
  name: string;
  id: string;
  realName: string;

  constructor(){
  }
}
