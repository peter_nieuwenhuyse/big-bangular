import {Component, OnDestroy, OnInit} from '@angular/core';
import { Person } from '../person';
import { ActivatedRoute } from '@angular/router';
import { PersonsService } from '../persons.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/empty';
import 'rxjs/add/operator/switchMap';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-person-detail',
  templateUrl: './person-detail.component.html',
  styleUrls: ['./person-detail.component.css']
})
export class PersonDetailComponent implements OnInit, OnDestroy {
  errorMessage: string;
  person: Person;
  subscriptionBag: Subscription;

  constructor(private route: ActivatedRoute,
              private personsService: PersonsService) {
    this.subscriptionBag = new Subscription();
  }

  ngOnInit() {
    const personSub = this.route.paramMap.map(paramMap => paramMap.get('id')).switchMap(id => {
      if (id) {
        return this.personsService.getPersonDetail(id);
      } else {
        return Observable.empty<Person>();
      }
    }).subscribe(person => this.person = person);

    this.subscriptionBag.add(personSub);
  }

  ngOnDestroy() {
    this.subscriptionBag.unsubscribe();
  }
}
