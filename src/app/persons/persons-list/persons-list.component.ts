import { Component, OnInit } from '@angular/core';
import { PersonsService } from '../persons.service';
import { Person } from '../person';

@Component({
  selector: 'app-persons-list',
  templateUrl: './persons-list.component.html',
  styleUrls: ['./persons-list.component.css']
})
export class PersonsListComponent implements OnInit {
  persons: Person[] = [];
  errorMessage: string;
  constructor(private personsService: PersonsService) { }

  delete(person: Person) {
    if (!confirm('Are you sure you want to delete this actor?')) { return; }
    this.personsService.deletePerson(person.id) ;
    this.persons.splice(this.persons.indexOf(person), 1);
  }

  ngOnInit(): void {
    this.personsService.getPersonsList()
      .subscribe(persons => {
        this.persons = persons;
      },
        error => this.errorMessage = 'error');
  }
}
