interface ConvertBackend extends Person{
  personList: Person[];
}

export interface Person {
    lastName: string;
    profession: string;
    bio: string;
    url: string;
    imageUri: string;
    name: string;
    id: string;
    realName: string;
}


