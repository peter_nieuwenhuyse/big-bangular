import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import { Person } from './person';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import {ErrorObservable} from 'rxjs/observable/ErrorObservable';
import { PersonModel } from './models/person.model';
import {Headers} from '@angular/http';


@Injectable()

export class PersonsService {
  private _personsUrl = '/api/persons.json';
  private personUrl = '/api/persons/';
  private pushUrl = '/api/persons';
  private personPlaceholder = new PersonModel();
  private headers = new Headers({'Content-Type': 'application/json'});
  constructor(private _http: HttpClient) { }

  getPersonsList(): Observable<Person[]> {
    return this._http.get<PersonListContainer>(this._personsUrl).map(personListContainer => personListContainer.personList)
      .catch(this.handleError);
  }

  getPersonDetail(id: string): Observable<Person> {
    const url: string = this.personUrl + id + '.json';
    return this._http.get<PersonContainer>(url).map(personContainer => personContainer.person)
      .catch(this.handleError);
  }

  updatePerson(person: Person): Observable<Person> {
    this.personPlaceholder.person = person;
    return this._http.put(this.pushUrl, this.personPlaceholder, this.headers)
      .catch(this.handleError);
  }

  createPerson(person: Person): Observable<Person> {
    this.personPlaceholder.person = person;
    return this._http.post<PersonContainer>(this.pushUrl, this.personPlaceholder, this.headers)
      .catch(this.handleError);
  }
  deletePerson(id: string): Observable<null> {
    const url: string = this.personUrl + id;
    return this._http.delete(url).map(value => null);
  }

  private handleError(err: HttpErrorResponse): ErrorObservable {
    let errorMessage = '';
    if (err.error instanceof Error) {
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      errorMessage = `Server returned code: ${err.status}, error message is : ${err.message}`;
    }
    console.log(errorMessage);
    return Observable.throw(errorMessage);
  }
}

interface PersonListContainer {
  personList: [Person];
}

interface PersonContainer {
  person: [Person];
}
